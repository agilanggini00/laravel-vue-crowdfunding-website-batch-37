<?php
trait Hewan{
    public $nama;
    public $darah = 50;
    public $JumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}";
    }
}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        echo "<br>";
        echo "{$this->nama} sedang menyerang {$hewan->nama}";
        $hewan->diserang($this);
    }

    public function diserang($hewan)
    {
        echo "<br>";
        echo "{$this->nama} sedang diserang {$hewan->nama}";
        $this->darah = $this->darah - ($hewan->attackPower/ $hewan->defencePower);
    }

    protected function getinfo()
    {
        echo "<br>";
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah Kaki : {$this->jumlahKaki}";
        echo "<br>";
        echo "Keahlian  : {$this->keahlian}";
        echo "<br>";
        echo "Bar Darah : {$this->darah}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "DefencePower : {$this->defencePower}";
        echo "<br>";
        $this->atraksi();
    }

    abstract public function getinfoHewan();
}

class Elang extends Fight{
    public function __construct($string){
        $this->nama = $string;
        $this->jumlahKaki= 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }
    public function getinfoHewan(){
        echo "Nama Hewan : Elang";
        $this->getinfo();
    }
}

class Harimau extends Fight{
    public function __construct($string){
        $this->nama = $string;
        $this->jumlahKaki= 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }
    public function getinfoHewan(){
        echo "Nama Hewan : Harimau";
        $this->getinfo();
    }
}

class spasi {
    public static function tampilkan(){
        echo "<br> <br>";
    }
}

$elang = new Elang("elang");
$elang->getinfoHewan();
spasi::tampilkan();
$hari= new harimau ("harimau");
$hari->getinfoHewan();
$hari-> serang ($elang);
spasi::tampilkan();
$elang->getinfoHewan();
$elang-> serang ($hari);
spasi::tampilkan();
$hari->getinfoHewan();

?>
